import ast
import math
import glob
import sys

inputfile=open(sys.argv[1],'r')



wordoccurance={}

while True:
	temp1=inputfile.readline()
	if 'done' in temp1:
		break
	temp2=inputfile.readline()
	wordoccurance.update({temp1:ast.literal_eval(temp2)})



uniquewordcount=0
while True:
	temp1=inputfile.readline()
	if 'done' in temp1:
		break
	uniquewordcount=temp1


messagecount={}
while True:
	temp1=inputfile.readline()
	if 'done' in temp1:
		break
	temp2=inputfile.readline()
	messagecount.update({temp1:temp2})

classswordcount={}
while True:
	temp1=inputfile.readline()
	if 'done' in temp1:
		break
	temp2=inputfile.readline()
	classswordcount.update({temp1:temp2})

classsprobability={} 

summationofmessagecount=0
for number in messagecount.keys():
	summationofmessagecount+=int(messagecount.get(number))


#open a test file-make a vector of its contents(what about duplicate words)
outputfile=open('STDOUT','w')
testfile=open(sys.argv[2],'r',encoding='cp1252', errors='ignore')



for line in testfile:	

	wordlist=line.split('\n')[0].split(' ')
	#print(wordlist)
		#for dev
	#wordlist.remove(wordlist[0])

	for classs in classswordcount.keys():
		classsprobability.update({classs:0})

	for word in wordlist:
		#print('now for this word: '+word)
		for classs in classswordcount.keys():
			
			if word in 	wordoccurance.get(classs).keys():
				#print('classprobability 1: '+classs+str(classsprobability.get(classs)))
				#print('word occurance for word: '+word+' '+classs+str(wordoccurance.get(classs).get(word)+1))
				#print('denominator: ' +str(float(classswordcount.get(classs))+float(uniquewordcount)))
				#print('sum so far: '+str(math.log(float(wordoccurance.get(classs).get(word))+1)-math.log(float(classswordcount.get(classs))+float(uniquewordcount))))
				#print('classprobability 2: '+classs+str(classsprobability.get(classs)))
				classsprobability.update({classs:float(classsprobability.get(classs))+math.log(float(wordoccurance.get(classs).get(word))+1)-math.log(float(classswordcount.get(classs))+float(uniquewordcount))})
				#print('classprobability 3: '+classs+str(classsprobability.get(classs)))
			else:
				#print('not found in the class: '+classs+' '+word)
				classsprobability.update({classs:float(classsprobability.get(classs))+math.log(1)-math.log(float(classswordcount.get(classs))+float(uniquewordcount))})


	
	for classs in classsprobability.keys():			
		classsprobability.update({classs:float(classsprobability.get(classs))+math.log(float(messagecount.get(classs)))-math.log(summationofmessagecount)})
		
	

	winnerclass=''
	for firstkey in classsprobability.keys():	
		winnerprobability=classsprobability.get(firstkey) #initialize the probability with any key
		winnerclass=firstkey
		break


	for classs in classsprobability.keys():
		#outputfile.write(str(classsprobability.get(classs)))
		if winnerprobability<classsprobability.get(classs):
			winnerprobability=classsprobability.get(classs)
			winnerclass=classs

	outputfile.write(winnerclass)



