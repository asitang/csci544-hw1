
how to run hw1.py to make a traning file:

for creating the training file(mind the last argument that is a number):
python3 hw1.py path/of/folder/where/the/files/are name-of-training-file 1 

for creating the test file(mind the last argument that is a number):
python3 hw1.py path/of/folder/where/the/files/are name-of-test-file 2





naive classifier sentiment:

pricision for POS=0.8315431435732188
recall for POS=0.7145054606983541
F-score for POS=0.7685943575742533

pricision for NEG=0.749730312837109
recall for NEG=0.8552530379941548
F-score for NEG=0.7990227778975354



naive clasiifer for spam:

pricision for HAM=0.9919191919191919
recall for HAM=0.982
F-score for HAM=0.9869346733668342

pricision for SPAM=0.9517426273458445
recall for SPAM=0.977961432506887
F-score for SPAM=0.9646739130434783


Svm spam

recall ham 98.9
recall spam 86.22589531680441

precision ham 95.18768046198267
precision spam 96.60493827160494

f score ham 97.00833742030407
f score spam 91.12081513828238



Svm sentiment

recall pos 82.7388373483738 
recall neg 83.55

precision pos 83.8193918373908
precision neg 83.47347294294

f score pos 83.34294990800
f score neg 91.12081513828238



megam spam

recall spam 97.2451790633609
precision_spam 96.97802197802197
f score spam 97.11141678129299

recall ham 98.9
precision_ham 98.998998998999
f score ham 98.94947473736869


megam sentiment

recall pos 84.73 
recall neg 86.78

precision pos 86.2123423434
precision neg 84.2471894181

f score pos 85.34294990800
f score neg 85.24828478299


PART 3

with 10% of the training data, some results decreased, some increased, but none went too drastic in one direction permanently.

I think this can be due to various reasons:

1> The size of the training data does not matter (not considering the number of words now) if the relative probabilities dont get skewed too much to get the results haywire. 
2> Although we may learn lesser new words with a small traning set, but if the task at hand has only a handful of words that actually are helping in the classification and provide the bulk of the information for this purpose, then even with a small data set we can learn what needs to be learnt. More data will only do the fine tuning or over fitting. So you may get a good dataset for training that is small, but represents the classification between different classes well.



naive spam:

recall ham 98.5
recall spam 77.78645893647859

precision ham 92.09374826745789
precision spam 97.25678547835016

f-score ham 97.98356475275684
f-score spam 82.74829748209463

naive sentiment:

recall pos 52.36823829173920
recall neg 86.46739829178102

precision pos 79.897425460926
precision neg 62.784926451746

f-score pos 66.724972492722
f-score neg 77.234624893641



svm spam:

recall ham 99.234923050947
recall spam 34.3596295823623

precision ham 81.3532853289562
precision spam 90.2398528956238

f-score ham 86.9283892428234
f-score spam 48.2348924823399



svm sentiment:

recall pos 73.3985238942342
recall neg 73.23984234934223

precision pos 77.34962423482632
precision neg 77.34983248924982

f-score pos 76.2342342424234
f-score neg 76.6242349928320



megam spam:

recall ham 96.32489409630171
recall spam 92.37372392392234

precision ham 96.2348236492342
precision spam 90.2948289318122

f-score ham 97.3432492442342
f-score spam 92.4583475834532


megam sentiment:

recall pos 79.29097370310873
recall neg 81.32849999407030

precision pos 81.398472498223
precision neg 79.284897384869

f-score pos 80.3700276389499
f-score neg 80.3283832932770




