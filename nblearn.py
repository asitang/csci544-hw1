import sys

inputfile=open(sys.argv[1],'r')
outputfile=open(sys.argv[2],'w')

wordset=set()

# a dictionary for class name and corresponding total word count, one for count of messges in that class, one for dictionary of word occurance frequency

classswordcount={}
messagecount={}
wordoccurance={} #a dictionary of dictionary

for line in inputfile:
	wordlist=line.split('\n')[0].split(' ')
	classs=wordlist[0]
	
	if classs in messagecount.keys():
			messagecount.update({classs:messagecount.get(classs)+1})
			wordlist.remove(classs)
			for word in wordlist:
				classswordcount.update({classs:classswordcount.get(classs)+1})
				if word in wordoccurance.get(classs).keys():
					wordoccurance.get(classs).update({word:wordoccurance.get(classs).get(word)+1})
				else:
					wordoccurance.get(classs).update({word:1})	

	else:
			messagecount.update({classs:1})
			wordlist.remove(classs)
			wordoccurance.update({classs:{}}) #create an empty class dictionary in the mother dictionary
			classswordcount.update({classs:0})
			for word in wordlist:
				classswordcount.update({classs:classswordcount.get(classs)+1})
				if word in wordoccurance.get(classs).keys():
					wordoccurance.get(classs).update({word:wordoccurance.get(classs).get(word)+1})
				else:
					wordoccurance.get(classs).update({word:1})	

	wordset.update(wordlist)					
		

for classs in wordoccurance.keys():
	outputfile.write(str(classs)+'\n')
	outputfile.write(str(wordoccurance.get(classs))+'\n')
outputfile.write('done'+'\n')


outputfile.write(str(len(wordset))+'\n')
outputfile.write('done'+'\n')

for classs in messagecount.keys():
	outputfile.write(str(classs)+'\n')
	outputfile.write(str(messagecount.get(classs))+'\n')
outputfile.write('done'+'\n')
	

for classs in classswordcount.keys():
	outputfile.write(str(classs)+'\n')
	outputfile.write(str(classswordcount.get(classs))+'\n')
outputfile.write('done'+'\n')



	


